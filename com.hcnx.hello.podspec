#
# Be sure to run `pod lib lint com.hcnx.hello.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'com.hcnx.hello'
  s.version          = '3.0.9'

  s.summary          = 'HCNX Hello.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
Intregration of Hello via CocoaPod.
                       DESC

  s.homepage         = 'https://bitbucket.org/devmobile/frameworkhello-ios'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Guillaume MARTINEZ' => 'g.martinez@highconnexion.com' }
  s.source           = { :git => 'https://bitbucket.org/devmobile/frameworkhello-ios.git', :tag => s.version.to_s }

  s.ios.deployment_target = '9.0'

  s.ios.vendored_frameworks = '*.framework'
  s.dependency 'com.hcnx.hcnx_base'

end
