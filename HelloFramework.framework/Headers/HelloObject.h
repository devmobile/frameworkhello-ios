//
//  HelloObject.h
//  HelloFramework
//
//  Created by Guillaume MARTINEZ on 05/12/2016.
//  Copyright © 2016 HighConnexion. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HelloObject : NSObject

- (id)initWithDictionary:(NSDictionary*)pDic;

@property (nonatomic, readonly) NSInteger segment_category_order;
@property (nonatomic, readonly) NSInteger segment_order;
@property (nonatomic, retain, readonly) NSString * segment_color;
@property (nonatomic, readonly) BOOL segment_is_default;
@property (nonatomic, readonly) BOOL segment_is_visible;
@property (nonatomic, readonly) BOOL segment_is_subscribed;
@property (nonatomic, retain, readonly) NSString * segment_name;
@property (nonatomic, retain, readonly) NSString * segment_code;
@property (nonatomic, retain, readonly) NSString * segment_category;
@property (nonatomic, retain, readonly) NSDictionary * data;

@end
