//
//  Hello.h
//  HelloFramework
//
//  Created by Guillaume MARTINEZ on 25/05/2016.
//  Copyright © 2016 Guillaume MARTINEZ. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <HCNXBase/HCNXBase.h>
#import "HelloObject.h"

/*!
 @file Hello
 @class Hello
 @discussion Hello is a singleton used to register to Hello server and to subsribe or unsubsribe to channels<br>
 To use this singleton, you need :<br>
 - an API_KEY provided by Highconnexion<br>
 - a HMAC_KEY provided by Highconnexion<br>
 - an tokenDevice provide by Apple
 @version 1.0
 @author contact.supportpro@highconnexion
 */


@interface Hello : HCNXBase <MFMessageComposeViewControllerDelegate, MFMailComposeViewControllerDelegate, SFSafariViewControllerDelegate>

/*!
 @brief Use this method to get the instance of Hello
 */
+ (Hello *)sharedInstance;

/*!
 @brief Use this method to ask the notification push permission
 */
- (void)askPermission;

/*!
 @brief Use this method to set the Apple tokenDevice
 @param pTokenDevice is an NSString provide by Apple
 */
- (void)setTokenDevice:(NSData*)pTokenDevice;

/*!
 @brief Use this method return the Apple tokenDevice
 @return NSString mToken
 */
- (NSString*)getTokenDevice;

- (void)showLog:(BOOL)pActivated;

- (void)clearConfiguration;

/*!
 @brief Refresh the list of the channels for this app, with subscription status
 @return NSArray * segments the list of all the segments for this app, NSError * error if an error occured
 */
- (void)getSegments:(void (^)(NSArray * segments, NSError * error))onCompletion;

- (void)getChannels:(void (^)(NSArray * channels, NSError * error))onCompletion __deprecated;


/*!
 @brief Manage channels
 @param subChannelList is an NSArray fill with channel_code, list of all channels code that should be subscribed. Could be nil.
 @param unSubChannelList is an NSArray fill with channel_code, list of all channels code that should be subscribed. Could be nil.
 @brief if both of the parameters are fill the unSubscribe method will be executed
 @return NSError * error if an error occured
 */
- (void)manageSegments:(NSArray*) subSegmentList andUnSubSegmentList:(NSArray*)unSubSegmentList completion:(void (^)(NSError* error)) onCompletion;

- (void)manageChannels:(NSArray*) subChannelList andUnSubChannelList:(NSArray*)unSubChannelList completion:(void (^)(NSError* error)) onCompletion __deprecated;

- (void)getHours:(void (^)(NSArray * hours, NSError * error))onCompletion;

- (void)manageHours:(NSArray*) hourList andUnSubHour:(NSArray*) unSubHourList completion:(void (^)(NSError* error)) onCompletion;

- (id)handleRedirect:(NSDictionary *) payLoad;

- (void)getCampaignsInApp:(void (^)(NSArray * campaigns, NSError * error))onCompletion;

- (void)getMyPushOnCompletion:(void (^)(NSArray * myPush, NSError * error))onCompletion;

@end
